# taxonomy-common

## Installation

Use clj git dependency for this repo, e.g.

```clojure
se.jobtechdev.taxonomy/common {:git/url "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common.git"
                               :git/tag "..."
                               :git/sha "..."}
```

See [tags](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common/-/tags) page
for latest git tags and shas.

## New versions

Run `./release.sh` on master branch to publish a new "release". It will output the latest dependency coordinate.

## License

Licensed under the Eclipse Public License, Version 2.0