(ns jobtech-taxonomy-common.relation-test
  (:require [clojure.test :refer :all]
            [jobtech-taxonomy-common.relation :as relation]))

(deftest reverse-txs-test
  (doseq [[type reverse-type] relation/relations
          ;; remove symmetric relation like related<->related
          :when (not= type reverse-type)]
    (is (= (relation/edge-tx "from_xxx_xxx" type "toxx_xxx_xxx")
           (relation/edge-tx "toxx_xxx_xxx" reverse-type "from_xxx_xxx")))))

(deftest ids-test
  (testing "Reversed direction ids are the same even for symmetric relations"
    (doseq [[type reverse-type] relation/relations]
      (is (= (relation/id "from_xxx_xxx" type "toxx_xxx_xxx")
             (relation/id "toxx_xxx_xxx" reverse-type "from_xxx_xxx"))))))
